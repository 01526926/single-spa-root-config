FROM node:10-alpine as build

WORKDIR /app
COPY package*.json /app/

RUN npm install


COPY . /app
RUN sed -i "s|./importmap.json|https://storage.googleapis.com/mf_static_files/importmap.json |g" /app/src/index.ejs
RUN sed -i "s|http://localhost:9000/tuwien-root-config.js|http://35.234.97.149/tuwien-root-config.js |g" /app/src/index.ejs

ENTRYPOINT ["npm", "start"]
